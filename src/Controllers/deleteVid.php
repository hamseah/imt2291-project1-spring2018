<?php



require_once '../twig/vendor/autoload.php';
require_once "../DBmodel/playlist.php";


$loader = new Twig_Loader_Filesystem('./../View/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

session_start();
if($_SESSION['logedIn']){
    $id = $_GET['id'];
    $play = new Playlist();
    $res = $play->deleteVid($id);
    if($res['status'] == 'OK'){
        echo "Video ble slettet";
    } else{
        echo "Video ble IKKE slettet";
    }
    $res['id'] = $_SESSION['id'];
    $res['fname'] = $_SESSION['fname'];
    $res['lname'] = $_SESSION['lname'];
    $res['person'] = $_SESSION['Teacher'];
    $res['videos'] = $play->teacherPlaylist($res['id']);
    echo $twig->render('teacher_playlist.html', $res);
}

