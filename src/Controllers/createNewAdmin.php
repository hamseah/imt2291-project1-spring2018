<?php


require_once '../twig/vendor/autoload.php';
require_once "../DBmodel/admin.php";


$loader = new Twig_Loader_Filesystem('./../View/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (!isset($_POST['fname'])) {
  echo $twig->render('createNewAdmin.html', array());
} else {
  session_start();
  $data['fname'] = $_POST['fname'];
  $data['lname'] = $_POST['lname'];
  $data['email'] = $_POST['email'];
  $data['password'] = $_POST['password'];
  $data['createdBy'] = $_SESSION['adminId'];

  $newAdmin = new Admin();
  $res = $newAdmin->createNewAdmin ($data);
  $res['data'] = $data;
  if($res['status'] == 'OK'){
    echo "New admin created successfully!";
    echo $twig->render('adminCreated.html', $res); 
  } else {
    echo "Failed to create admin! Try again..";
    echo $twig->render('createNewAdmin.html', array());
  }
  
}