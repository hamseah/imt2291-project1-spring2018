<?php

require_once '../twig/vendor/autoload.php';
require_once "../DBmodel/video_upload.php";
require_once "../DBmodel/playlist.php";

$loader = new Twig_Loader_Filesystem('./../View/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

session_start();


if($_SESSION['logedIn']){
    if(!isset($_POST['submit'])){
        echo $twig->render('add_comment.html', array());
        $_SESSION['vidId'] = $_GET['id'];
    } else {
        $data['vidId'] = $_SESSION['vidId'];
        $data['id'] = $_SESSION['id'];
        $data['comment'] = $_POST['comment'];
        $data['person'] = $_SESSION['Teacher'];
        $video = new Video();
        $play = new Playlist();
        $res = $video->addComment($data);
        unset($_SESSION['vidId']);
        if($res['status'] == 'OK'){
            echo "kommentar ble addet";
        } else{
            echo "kommentar ble IKKE addet";
            print_r($res);
        }
        $res['id'] = $_SESSION['id'];
        $res['fname'] = $_SESSION['fname'];
        $res['lname'] = $_SESSION['lname'];
        $res['person'] = $_SESSION['Teacher'];
        if($res['person'] == 'student'){
            $res['videos'] = $play->getStudPlaylist($data['id']);
            echo $twig->render('show_stud_playlist.html', $res);
        } else {
            $res['videos'] = $play->teacherPlaylist($data['id']);
            echo $twig->render('teacher_playlist.html', $res);
        }
        
    }
}
