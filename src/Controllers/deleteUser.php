<?php



require_once '../twig/vendor/autoload.php';
require_once "../DBmodel/admin.php";
require_once 'adminPage.php';


$loader = new Twig_Loader_Filesystem('./../View/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));


$id = $_GET['id'];
$user = $_GET['user'];
$admin = new Admin();
$res = $admin->deleteUser($id, $user);
if($res['status'] == 'OK'){
    echo "Bruker ble slettet";
    $page = new AdminPage();
    $page->createPage();
} else{
    echo "Bruker ble IKKE slettet";
    $page = new AdminPage();
    $page->createPage();
}

