<?php

require_once '../twig/vendor/autoload.php';
require_once "../DBmodel/video_upload.php";

$loader = new Twig_Loader_Filesystem('./../View/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

session_start();
if(isset($_SESSION['logedIn'])){
    $res['id'] = $_SESSION['id'];
    $res['fname'] = $_SESSION['fname'];
    $res['lname'] = $_SESSION['lname'];
    $res['person'] = $_SESSION['Teacher'];
    $vid = new Video();
    $res['videos'] =  $vid->getAllVideos();
    
    echo $twig->render('showAllVid.html', $res);
} else {
    echo $twig->render('login.html', array());
}
