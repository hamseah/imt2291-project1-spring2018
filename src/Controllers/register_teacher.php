<?php

require_once '../twig/vendor/autoload.php';
require_once "../DBmodel/teacher_reg.php";


$loader = new Twig_Loader_Filesystem('./../View/twig_templates');
$twig = new Twig_Environment($loader, array(
    //'cache' => './compilation_cache',
));

if (!isset($_POST['fname'])) {
  echo $twig->render('addTeacherForm.html', array());
} else {
  $data['fname'] = $_POST['fname'];
  $data['lname'] = $_POST['lname'];
  $data['subject'] = $_POST['subject'];
  $data['email'] = $_POST['email'];
  $data['password'] = $_POST['password'];


  $teacher = new User();
  $res = $teacher->addTeacher ($data);
  $res['data'] = $data;

  echo $twig->render('userAdded.html', $res); 
}