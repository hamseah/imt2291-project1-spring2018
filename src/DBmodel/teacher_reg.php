<?php



class User{
    private $db;

    public function __construct(){
        try {
            $this->db = new PDO('mysql:host=localhost; dbname=prosjekt1; charset=utf8','root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo "Error accured creating new PDO..";
            echo $e->getMessage();
        }
    }
    
    /**
     * Stenger databasen når objektet dør
     */
    public function __destruct() {
        if ($this->db!=null) {
          unset ($this->db);
        }
    }


    /**
     * For registrering av student
     * @param: array av data om læreren som skal legges i db
     * @return: array av data for å sjekke om det oppstå feil eller ikke
     *  og for å skrive navnet på vedkommende som ble registrert
    **/
    public function addTeacher($data){
        
        $sql = 'insert into teachers (Fname, Lname, Email, Subject, reg_godkjent, Password) VALUES (?, ?, ?, ?, ?, ?)';
        $sth = $this->db->prepare ($sql);
        $pass = password_hash($data['password'], PASSWORD_DEFAULT);
        
        try {
            $sth->execute (array ($data['fname'], $data['lname'],
            $data['email'], $data['subject'], "NEI", $pass));
        
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        $tmp = [];
        if ($sth->rowCount()==1) {
            $tmp['status'] = 'OK';
            $tmp['id'] = $this->db->lastInsertId();
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into contact registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        return $tmp;
    }

    /***
     * For registrering av student
     * @param: array av data om studenten som skal legges i db
     * @returnerer array av data for å sjekke om det oppstå feil eller ikke
     *  og for å skrive navnet på vedkommende som ble registrert
    ***/
    public function addStudent($data){
        
        $sql = 'insert into students (Fname, Lname, Email, Favorit, reg_godkjent, Password) VALUES (?, ?, ?, ?, ?, ?)';
        $sth = $this->db->prepare ($sql);
        $pass = password_hash($data['password'], PASSWORD_DEFAULT);
        
        try {
            $sth->execute (array ($data['fname'], $data['lname'],
            $data['email'],$data['fav'], "NEI" ,$pass));
        
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert student into db';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        $tmp = [];
        if ($sth->rowCount()==1) {
            $tmp['status'] = 'OK';
            $tmp['id'] = $this->db->lastInsertId();
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert student into db';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        return $tmp;
    }

    /**
     * Håndterer innlogging for lærere
     * @param: array med innlogginsdata(epost, passord)
     * @return: returnerer evt feilmeld til useren
     */

     public function loginTeacher($data){        
        $tmp = [];

        try {
            $sql = 'SELECT id, Password, Fname, Lname, reg_godkjent FROM teachers WHERE Email = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array ($data['email']));
            $data_array = $sth->fetch(PDO::FETCH_ASSOC);
            if(password_verify($data['password'], $data_array['Password']) && $data_array['reg_godkjent'] == "JA"){
                $tmp['status'] = 'OK';
                $tmp['id'] = $data_array['id'];
                $tmp['fname'] = $data_array['Fname'];
                $tmp['lname'] = $data_array['Lname'];
                $tmp['person'] = "Teacher";
            } else{
                $tmp['status'] = 'FAIL';
                $tmp['errorInfo'] = 'Wrong email or password';
            }
            
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert student into db';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        return $tmp;  
     }
     
    /**
     * Håndterer innlogging for student
     * @param: array med innlogginsdata(epost, passord)
     * @return: returnerer evt feilmeld til useren
     */
     public function loginStudent($data){
        $tmp = [];

        try {
            $sql = 'SELECT id, password, Fname, Lname, reg_godkjent FROM students WHERE Email = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array ($data['email']));
            $data_array = $sth->fetch(PDO::FETCH_ASSOC);
            if(password_verify($data['password'], $data_array['password']) && $data_array['reg_godkjent'] == "JA"){
                $tmp['id'] = $data_array['id'];
                $tmp['fname'] = $data_array['Fname'];
                $tmp['lname'] = $data_array['Lname'];
                $tmp['person'] = "Student";
                $tmp['status'] = 'OK';
            } else{
                $tmp['status'] = 'FAIL';
            }
            
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert student into db';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        return $tmp;
        
     }



};