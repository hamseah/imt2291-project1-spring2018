<?php

/**
 * Skal håndtere vid administrerering til og fra db
 */

 class Video{
    private $db;

    /**
     * Contructoren kobler til db med PDO
     */
    public function __construct(){
        try {
            $this->db = new PDO('mysql:host=localhost; dbname=prosjekt1; charset=utf8','root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo "Error accured creating new PDO..";
            echo $e->getMessage();
        }
    }

    /**
     * Stenger kobling til db når objektet dør
     */
    public function __destruct() {
        if ($this->db!=null) {
          unset ($this->db);
        }
    }

    /**
     * @param: array med vidoe- data som skal sendes til db
     */
    public function addVideo($data){
        $tmp = [];

        try{
            $sql = 'Select Fname, Lname, Subject from teachers where id= ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute(array($data['id']));
            $t_info = $sth->fetch(PDO::FETCH_ASSOC);
            //print_r($t_info);
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to select teacher from db into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        
        try {
            $sql = 'insert into videos (Lecturer, EmneCode, link, Topic, type, teacher_id) VALUES (?, ?, ?, ?, ?, ?)';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array ($t_info['Fname'].' '.$t_info['Lname'], $t_info['Subject'],
            $data['destination'], $data['topic'], $data['type'], $data['id']));
        
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert Video into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        if ($sth->rowCount()==1) {
            $tmp['status'] = 'OK';
            $tmp['id'] = $this->db->lastInsertId();
            $tmp['person'] = "Teacher";
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert video into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        return $tmp;
    }

    /**
     * Skal hente alle videone
     * @return: array av alle videoe.
     */
    public function getAllVideos(){
        $tmp = [];

        try{
            $sql = 'Select * from videos';
            $sth = $this->db->prepare ($sql);
            $sth->execute();
            $tmp['vid'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $sql = 'Select videoId, rate, comments from videoinfo';
            $sth = $this->db->prepare ($sql);
            $sth->execute(array());
            $tmp['videoInfo'] = $sth->fetchAll(PDO::FETCH_ASSOC);

        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        return $tmp;
    }

    /**
     *  Legger inn rate på video fra studenter
     *  @param array: id til studenten, id til videoen og rate (0-5)
     *  @return array: med status, errormelding.
     */
    public function addRate($data){
        $tmp = [];
        try {
            $sql = 'INSERT INTO videoinfo (videoId, studentId, rate) VALUES (?, ?, ?)';
            $sth = $this->db->prepare ($sql);
            $sth->execute(array($data['vidId'], $data['id'], $data['rate']));
            $tmp['status'] = 'OK';
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert rating into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        } 
        return $tmp;
    }

    /**
     *  Legger inn kommentarer fra brukerne inn i db
     *  @param array: id til brukeren pluss id til video og kommentaren
     *  @return array: med status, errormelding.
     */
    public function addComment($data){
        $tmp = [];
        try {
            if($data['person'] == 'student'){
                $sql = 'INSERT INTO videoinfo (videoId, studentId, comments) VALUES (?, ?, ?)';
                $sth = $this->db->prepare ($sql);
                $sth->execute(array($data['vidId'], $data['id'], $data['comment']));
            } else {
                $sql = 'INSERT INTO videoinfo (videoId, teacherId, comments) VALUES (?, ?, ?)';
                $sth = $this->db->prepare ($sql);
                $sth->execute(array($data['vidId'], $data['id'], $data['comment']));
            }
            $tmp['status'] = 'OK';
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert Comment into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        } 
        return $tmp;
    }

 };