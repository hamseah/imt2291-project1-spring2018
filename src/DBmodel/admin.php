<?php


/**
 * Classe for Admin brukere, skal utføre det admin handlinger ut mot databasen
 * 
 */

 class Admin{
    private $db;

    /**
     * Contructoren kobler til db med PDO
     */
    public function __construct(){
        try {
            $this->db = new PDO('mysql:host=localhost; dbname=prosjekt1; charset=utf8','root', '');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (Exception $e) {
            echo "Error accured creating new PDO..";
            echo $e->getMessage();
        }
    }

    /**
     * Stenger databasen når objektet dør
     */
    public function __destruct() {
        if ($this->db!=null) {
          unset ($this->db);
        }
    }

    /**
     * Legger inn ny admin i databasen
     * @param: array med dataene til den nye admin
     * @return: array med tilbakemeldinger.
     */
    public function createNewAdmin($data){
        $tmp = [];
        try{
            $sql = 'insert into admin (first_name, last_name, email, password, created_by) values (?, ?, ?, ?, ?)';
            $sth = $this->db->prepare($sql);
            $sth->execute (array($data['fname'], $data['lname'], $data['email'], password_hash($data['password'], PASSWORD_DEFAULT)
            ,$data['createdBy']));
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        if($sth->rowCount() == 1){
            $tmp['status'] = 'OK';
            $tmp['id'] = $this->db->lastInsertId();
        } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into contact registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        return $tmp;
    }

    /**
     *  Funksjon som sletter en admin
     *  @param integer: id til admin som skal bli slettet
     *  @return array: Status av slettingen.
     */
    public function deleteAdmin($id){
        $tmp = [];

        try{
            $sql = 'DELETE FROM admin where id = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array($id));
            $tmp['status'] = 'OK';            
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
        return $tmp;
    }


    /**
     * For innlogging av admins.
     * @param: array med data (epost og passord)
     * @return: array med status-info osv for å sjekker om innloggingen var vellykket
     */
    public function logInAdmin($data){
        $tmp = [];

        try {
            $sql = 'SELECT id, password, first_name, last_name FROM admin WHERE email = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array ($data['email']));
            $data_array = $sth->fetch(PDO::FETCH_ASSOC);
            if(password_verify($data['password'], $data_array['password'])){
                $tmp['status'] = 'OK';
                $tmp['id'] = $data_array['id'];
                $tmp['fname'] = $data_array['first_name'];
                $tmp['lname'] = $data_array['last_name'];
                $tmp['person'] = "Admin";
            } else{
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Wrong email or password';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
            
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Wrong email or password';
            $tmp['errorInfo'] = $sth->errorInfo();
        }

        return $tmp;  
    }    
    
    /**
     * Gets all userdata(Teachers and students data from db)
     * @return: array with user- data.
     */
    public function getAllUserData(){
        $tmp = [];

        try{
            $sql = 'SELECT * FROM teachers';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array());
            
            $tmp['teachers'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $tmp['count']  = count($tmp['teachers']);
    
            $query= 'SELECT * FROM students';
            $sth= $this->db->prepare($query);
            $sth->execute(array());
    
            $tmp['students'] = $sth->fetchAll(PDO::FETCH_ASSOC);
            $tmp['count2'] = count($tmp['students']);
            $tmp['status'] = 'OK';
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
       
        return $tmp;

    }

    /**
     * Skal finne en lærer som skal slettes
     * @param: id til læreren
     * @return: all info læreren tilbake til admin
     */
    public function findTeacher($id){
        $tmp = [];

        try{
            $sql = 'SELECT * FROM teachers where id = ?';
            $sth = $this->db->prepare ($sql);
            $sth->execute (array($id));
            
            $tmp['founds'] = $sth->fetchAll(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Failed to insert teacher into registry';
            $tmp['errorInfo'] = $sth->errorInfo();
        }
       
        return $tmp;
    }

    /**
     * Deleting a user
     * @param: id of the user and the type of user (teacher/student).
     * @return: status of the deletion.
     */
    public function deleteUser($id, $user){
        $tmp = [];
        if($user == 'teacher'){
            try{
                $sql = 'DELETE FROM teachers where id = ?';
                $sth = $this->db->prepare ($sql);
                $sth->execute (array($id));
                
            } catch (Exception $e) {
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Failed to insert teacher into registry';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
    
            if($sth->rowCount() == 1){
                $tmp['status'] = 'OK';
                $tmp['id'] = $this->db->lastInsertId();
            } else {
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Failed to insert teacher into contact registry';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
        } else {
            try{
                $sql = 'DELETE FROM students where id = ?';
                $sth = $this->db->prepare ($sql);
                $sth->execute (array($id));
                
            } catch (Exception $e) {
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Failed to insert teacher into registry';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
    
            if($sth->rowCount() == 1){
                $tmp['status'] = 'OK';
            } else {
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Failed to insert teacher into contact registry';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
        }
        
       
        return $tmp;
    }

    /**
     * Approving new user(teacher or student)
     * @param: id and type of user.
     * @return: the result of the action(OK or FAIL)
     */
    public function approveUser($id, $user){
        $tmp = [];
        if($user == 'teacher'){
            try{
                $sql = 'UPDATE teachers set reg_godkjent = "JA" where id = ?';
                $sth = $this->db->prepare ($sql);
                $sth->execute (array($id));
                $tmp['status'] = 'OK';
                
            } catch (Exception $e) {
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Failed to insert teacher into registry';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
    
        } else {
            try{
                $sql = 'UPDATE students set reg_godkjent = "JA" where id = ?';
                $sth = $this->db->prepare ($sql);
                $sth->execute (array($id));
                $tmp['status'] = 'OK';
                
            } catch (Exception $e) {
                $tmp['status'] = 'FAIL';
                $tmp['errorMessage'] = 'Failed to insert teacher into registry';
                $tmp['errorInfo'] = $sth->errorInfo();
            }
        }
        
       
        return $tmp;
    }


 };