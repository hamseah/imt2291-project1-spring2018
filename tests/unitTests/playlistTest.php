<?php

require_once '../../src/DBmodel/teacher_reg.php';
require_once '../../src/DBmodel/admin.php';
require_once '../../src/DBmodel/playlist.php';

/**
 * class for testing playlist class.
 */

 class playlistTest extends PHPUnit_Framework_TestCase{
    
    private $playlist;
    private $student;
    private $teacher;
    private $user;
    private $admin;


    /**
     *  Runs before every tests and makes the setup for the tests
     *  inserting a student into db and connecting his favourite- field in db with an existing video subject
     */
    protected function setup(){
        try{
            $this->user = new User;
            $this->admin = new Admin;
            $this->playlist = new Playlist();

            //Student who will be used in the tests
            $this->student['lname'] = md5(date('l jS \of F Y h:i:s B'));  // Create random 32 character string
            $this->student['fname'] = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
            $this->student['fav'] = "imt2218";
            $this->student['email'] = $this->student['fname']."@".$this->student['lname']."no";
            $this->student['password'] = "123";
            $data = $this->user->addStudent($this->student);
            $this->student['id'] = $data['id'];

            //The teacher who will be used in the tests
            $this->teacher['lname'] = md5(date('l jS \of F Y h:i:s B'));  
            $this->teacher['fname'] = md5(date('l jS \of F Y h:i:s A'));  
            $this->teacher['subject'] = "imt2218";
            $this->teacher['email'] = $this->teacher['fname']."@".$this->teacher['lname']."no";
            $this->teacher['password'] = "123";
            $data2 = $this->user->addTeacher($this->teacher);
            $this->teacher['id'] = $data2['id'];
        } catch (Exception $e) {
            return "Fail". $e->getMessage();
        }
    }

    /**
     * This method is called after the last test of this test class is run.
     */
    protected function tearDown(){
        //Deleting the user student and teacher
        $this->admin->deleteUser($this->student['id'], 'student');
        $this->admin->deleteUser($this->teacher['id'], 'teacher');
        unset($this->user);
        unset($this->student);
        unset($this->teacher);
        unset($this->admin);
        unset($this->playlist);
    }

    /**
     * Testing that a student can get he's or she's playlist
     * Testing the function getStudPlaylist($id) in Playlist class
     */
    public function testThatPlaylistOfAStudentCanBeRetrieved(){
        $res = $this->playlist->getStudPlaylist($this->student['id']);
        $this->assertTrue($res['status'] == 'OK', 'Playlist couldt be retrieved');
        $this->assertNotEmpty($res['vid'], 'No videos was return');
    }
    /**
     * Testing that a teacher can get correct playlist
     * Testing the function getStudPlaylist($id) in Playlist class
     */
    public function testThatTeacherCanGetHisPlaylist(){
        $res = $this->playlist->teacherPlaylist($this->teacher['id']);
        $this->assertTrue($res['status'] == 'OK', 'Playlist couldt be retrieved');
        $this->assertNotEmpty($res['vid'], 'No videos was return');
    }
 }
